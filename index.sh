#!/bin/bash

# Run this script like this:
# bash <(curl -s https://bitbucket.org/raphizim/server-setup/raw/master/index.sh)
# OR
# bash <(curl -Ls http://tiny.cc/server_setup)

#
# CONFIGURATION
#==================================

# The fixed start of the IP address
IP_BASE=192.168.0.

# The name of the host to setup
HOST_NAME=$(hostname)

# The name/ip of the server which provides the host data
BASE_SERVER=192.168.0.50

#==================================

# Update the system
sudo apt-get update
sudo apt-get -y upgrade
sudo apt-get -y dist-upgrade

# Install nfs-common and git
sudo apt-get install -y nfs-common git

sudo mkdir /data/

# Clone the rest
git clone https://bitbucket.org/raphizim/server-setup.git data
cd data/

# Read in the IP to use
read -p "What's the IP of the new machine? $IP_BASE" IP_EXTENSION
IP="$IP_BASE$IP_EXTENSION"

# Set the static IP
cat etc_networking_interfaces | sed -e "s/{{IP}}/$IP/" > interfaces
sudo mv interfaces /etc/network/interfaces

# Automount /data directory
echo "$BASE_SERVER:/export/$HOST_NAME             /data     nfs     _netdev,defaults,auto,noatime,intr   0 0" | sudo tee -a /etc/fstab

# Cleanup
cd ../
rm -Rf ./data/

echo "DONE! Please reboot now!"
